# Eventtus Task
This is the Eventtus developer challenge created by **Abdelrahman Badary**


# Project architecture 
This project is created with the **MVVM** design pattern, i  choosed this pattern as it is light-weighted and fits the task at hand perfectly.
The project is divided into the following modules

- Models
- Views
- ViewModels
- Network 

Here is a brief description for each of the modules:

- ## Models
The models module contains 2 classes 

- **EventType**
- This class models the JSON response found in the following endpoint **/eventtypes**; The class contains a convenience init method to convert a json into an EventType object.

- **Event**
- This class models the JSON response found in the following endpoint **/events?event_type={event_type_id}&page={1,2,3}**; The class contains a convenience init method to convert a json into an Event object.

- ## Views
This module contains only 1 view controller 


- **EventsViewController**
- This class represents first wireframe in the task (Events screen), the controller contains 2 collection views, the first one is used to display the events types and the second one is used to display a list of items that is associated with a certain type. The first collection view displays a custome cell named "LabeledCollectionViewCell" - for generalization - this cell contains only 1 label and a method that populates the cell using its viewmodel. The second collection view has a nested cells structure, The main collection view cell is the "ItemsCollectionViewCell" and it contains within it a table view which also has a custom cell named "EventTableViewCell", here is a brief description of each cell 
- ItemsCollectionViewCell
- This cell contains a table view and implements the delegates and datasource methods of the table view. This class has a method called **loadDateWithViewModel** that takes as a parameter the view model associated to the current table view, this method awaits for 1 second than it tries to fetch the events data from the view model the reason for the 1 second delay is to make sure that the user is not fast swiping through the application, also in this class we implement the **scrollViewDidEndDragging** method when the user drags the table view to the end this class tries to load the second page of events.


- EventTableViewCell
- This the cell that displays the event data inside the table view, it contains only 1 method **loadDateWithViewModel** which takes as a parameter a view model and populates the cell using it.

The EventsViewController does not implement the collection view delegates and data source protocols instead there is a separate class (**EventTypesCollectionViewHandler**,**EventsCollectionViewHandler**) for each collection view, this approach helps to reuse the collection views in different areas and also keeps the view controller light-weighted. 

- ## ViewModels
The ViewModels module contains the following classes 


- **EventsViewModel**
- This class is the viewmodel for the whole **EventsViewController**, it contains only 1 public method **loadTypes**,  this method calls the types endpoint and creates 2 lists of viewmodels (the first list is of type **EventTypeViewModel** and the second one is of type **EventListViewModel**) from the response, each list is used with a collection view in the view controller.
- **EventTypeViewModel**
- This class is associated with the types collection view, it only contains a name property.
- **EventListViewModel**
- This class is associated with the events collection view, this class is initialized with an event_type_id and it has 2 public methods **loadItems** which calls the events endpoint with a specific type and a the default page, the second method is **loadNextPage** which loads the second page of the events, the two methods returns a list of viewmodel (ItemViewModel) which is used to populate the the table view cell.
- **ItemViewModel**
- This is the type of the list returned from the **EventListViewModel**  methods, this viewmodel class is used to populate the table view cells.

- ## Network 
The networking layer used in this project consists of 4 main parts



- Requests - a request holds all data related to a specafic request (HTTP method, headers, response type etc)

- Responses - a response is responsible for parsing the raw data it recieves and also it is responsible for converting Http errors and status codes to application specfic errors

- Tasks - a task represent an network operation on a specafic resource, the task is also responsible for parsing the json/data it recieves to the operation specfic data type.

- Dispatchers - the dispatcher is responsible for establishing the connection with the network, and also responsible for constructing a URLRequest out of the request object

You can read more about this archtiecture in the following medium [article](https://medium.com/@danielemargutti/network-layers-in-swift-7fc5628ff789 )




- ## Notes 
Delegation is used instead of binding thorugh out the application because delegation is commonly used in iOS and also it requires no third party use.


