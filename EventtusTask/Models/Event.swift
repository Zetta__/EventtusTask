//
//  Event.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class Event {
    //MARK: Properties
    var eventId: String
    var name: String
    var coverURL: URL?
    var startDate: String
    var endDate: String
    var latitude: Double?
    var longitude: Double?
    
    //MARK: inits
    init(eventId: String, name: String, coverURL: URL?, startDate: String, endDate: String, latitude: Double?, longitude: Double?) {
        self.eventId = eventId
        self.name = name
        self.coverURL = coverURL
        self.startDate = startDate
        self.endDate = endDate
        self.latitude = latitude
        self.longitude = longitude
    }
    
    convenience init(with json: [String: Any]){
        let eventId = json["id"] as! String
        let name = json["name"] as! String
        let coverString = json["cover"] as? String
        var coverURL: URL?
        if let coverString = coverString{
            coverURL = URL(string: coverString)
        }
        let startDate = json["start_date"] as! String
        let endDate = json["end_date"] as! String
        
        let latitude = json["latitude"] as? String
        var latitudeVal: Double?
        if let latitude = latitude{
            latitudeVal = Double(latitude)
        }
      
        let longitude = json["longitude"] as? String
        var longitudeVal: Double?
        if let longitude = longitude{
            longitudeVal = Double(longitude)
        }

        self.init(eventId: eventId, name: name, coverURL: coverURL, startDate: startDate, endDate: endDate, latitude: latitudeVal, longitude: longitudeVal)
    }
}
