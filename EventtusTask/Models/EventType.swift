//
//  EventType.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class EventType{
    //MARK: Properties
    var typeId: String
    var name: String
    
    //MARK: inits
    init(typeId: String, name: String) {
        self.typeId = typeId
        self.name = name
    }
    
    convenience init(with json: [String:Any]){
        let id = json["id"] as! String
        let name = json["name"] as! String
        self.init(typeId: id, name: name)
    }
}
