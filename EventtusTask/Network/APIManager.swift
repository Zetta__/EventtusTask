//
//  APIManager.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class APIManager {
   
    //MARK: Properties
    private var dispatcher: Dispatcher
    static var testManager: APIManager{
        let enviorment = Environment("testing", "http://private-7466b-eventtuschanllengeapis.apiary-mock.com")
        let dispatcher = NetworkDispatcher(environment: enviorment)
        return APIManager(with: dispatcher)
    }
    

    //MARK: init and public methods (endpoints)
    init(with dis: Dispatcher) {
        self.dispatcher = dis
    }

    func getEventsTypes(compeletionBlock: @escaping (_ items: [EventType]?, _ error: NetworkError?)->())  {
        let task = GetEventTypesTask()
        task.execute(in: dispatcher) { (items, error) in
            compeletionBlock(items, error)
        }
    }
    func getEvents(type: String, page: Int, compeletionBlock: @escaping (_ items: [Event]?, _ error: NetworkError?)->())  {
        let task = GetEventsTask(eventsType: type, page: page)
        task.execute(in: dispatcher) { (items, error) in
            compeletionBlock(items, error)
        }
    }
}
