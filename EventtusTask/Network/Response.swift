//
//  Response.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 2/16/18.
//  Copyright © 2018 zetta. All rights reserved.
//

import Foundation
/// An enum that holds common network errors.
public enum NetworkError: Error{
    case noConnection
    case noData
    case notFound
    case unAuthorized
    case serverError
    case timeout
    case unknownError
}

/// This enum is responsible for extracting the request data from the network response, it is also responsible for translating any status code, network error, to app specfic errors.
public enum Response {
    //MARK: Cases
    case json(json: [[String: Any]]?)
    case data(data: Data)
    case error(error: NetworkError)
    
    //MARK: init
    init(_ response: (r: HTTPURLResponse?, data: Data?, error: Error?), for request: Request) {
        self = .json(json: nil)
        guard response.r?.statusCode == 200, response.error == nil else{
            if let error = response.error{
                let error = getNetworkError(from: error)
                self = .error(error: error)
            }
            else{
                if let r = response.r {
                let error = getNetworkError(from: r.statusCode)
                    self = .error(error: error)
                }
                else{
                    self = .error(error: .unknownError)
                }
            }
            return
        }
    
        
        guard let data = response.data else{
            self = .error(error: .noData)
            return
        }
        
        switch request.dataType {
        case .JSON:
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                self = .json(json: json)
            }
            catch{
                self = .error(error: .unknownError)
            }
 
        case .Data:
            self = .data(data: data)
        }
    }
    
    //MARK: Helper methods
    func getNetworkError(from statusCode: Int) -> NetworkError {
        switch statusCode {
        case 404:
            return .notFound
        case 401:
            return .unAuthorized
        case 500:
            return .serverError
        default:
            return .unknownError
            
        }
    }
    
    func getNetworkError(from error: Error) -> NetworkError {
        let error = error as NSError
        if error.domain == NSURLErrorDomain{
            switch error.code {
            case NSURLErrorNotConnectedToInternet:
                return .noConnection
            case NSURLErrorTimedOut:
                return .timeout
            default:
                return .unknownError
            }
        }
        return .unknownError
    }
}
