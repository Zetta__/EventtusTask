//
//  NetworkDispatcher.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 2/16/18.
//  Copyright © 2018 zetta. All rights reserved.
//

import Foundation
/// This class is where all the network stuff happens, it dispatches the request using URLSession and URLSessionDataTask. This class is responsible for preparing the URLRequest with the appropriate data recieved in a Request object.
public class NetworkDispatcher: Dispatcher{
    var environment: Environment
    var session: URLSession
    
    public required init(environment: Environment) {
        self.environment = environment
        self.session = URLSession(configuration: .default)
    }
    
    public func execute(request: Request, compeletion: @escaping (Response) -> ()) {
        let req = prepareURLRequest(for: request)
        session.dataTask(with: req) { (data, response, error) in
            let error = error
            let res = Response.init((response as? HTTPURLResponse, data, error), for:request)
                compeletion(res)
            }.resume()
    }

    

    
    private func prepareURLRequest(for request: Request)  -> URLRequest{
        var urlString = "\(environment.host)/\(request.path)"
        if request.pathContainsHost{
            urlString = request.path
        }
        else{
            urlString = "\(environment.host)/\(request.path)"
        }
        let url = URL(string: urlString)
        var urlRequest = URLRequest(url: url!)
        
        switch request.parameters {
        case .body(let params):
            if let params = params{
                urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .init(rawValue: 0))
            }
        case .url(let params):
            if let params = params{
                let queryParams = params.map({ (element) -> URLQueryItem in
                
                    return URLQueryItem(name:element.key, value:element.value as? String)
                })
                var components = URLComponents(string: urlString)
                components?.queryItems = queryParams
                urlRequest.url = components?.url
            }
    }
        
        environment.headers.forEach{ urlRequest.addValue($0.value as! String, forHTTPHeaderField: $0.key)}
        request.headers?.forEach{ urlRequest.addValue($0.value as! String, forHTTPHeaderField: $0.key)}

        
        urlRequest.httpMethod = request.method.rawValue
        return urlRequest
    }
}
