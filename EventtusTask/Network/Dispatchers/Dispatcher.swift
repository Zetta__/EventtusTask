//
//  Dispatcher.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 2/16/18.
//  Copyright © 2018 zetta. All rights reserved.
//

import Foundation
/// A struct that holds global network info such as headers (used by all requests), host and a name
public struct Environment{
    public var name: String
    public var host: String
    public var headers: [String: Any] = [:]
    
    public init(_ name: String, _ host: String){
        self.name = name
        self.host = host
    }
}

public protocol Dispatcher{
    init(environment: Environment)
    func execute(request: Request,  compeletion: @escaping (_ response: Response)->())

}
