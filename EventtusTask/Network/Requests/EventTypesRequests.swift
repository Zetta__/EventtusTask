//
//  EventTypesRequests.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
public enum EventTypesRequests: Request{
    case getTypes
   
    public var path: String{
        return "/eventtypes"            
    }
    
    public var pathContainsHost: Bool{
            return false

    }
    
    public var method: HTTPMethod{
        return .GET
        
    }
    
    public var parameters: RequestParameters{
        return .url(nil)
        
    }
    
    public var dataType: DataType{
        return .JSON
    }
    
    public var headers: [String : Any]?{
        return nil
    
    }
    
}
