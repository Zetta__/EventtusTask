//
//  EventRequests.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
public enum EventRequests: Request{
    case getEvents(type: String, page: Int)
    
    public var path: String{
        return "/events"
    }
    
    public var pathContainsHost: Bool{
        return false
        
    }
    
    public var method: HTTPMethod{
        return .GET
        
    }
    
    public var parameters: RequestParameters{
        switch self {
        case .getEvents(let type, let page):
            return .url(["event_type":type, "page":page])
        }
        
    }
    
    public var dataType: DataType{
        return .JSON
    }
    
    public var headers: [String : Any]?{
        return nil
        
    }
    
}
