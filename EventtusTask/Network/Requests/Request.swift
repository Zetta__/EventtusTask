//
//  Request.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 2/16/18.
//  Copyright © 2018 zetta. All rights reserved.
//

import Foundation

public enum HTTPMethod:String{
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
    case PATCH = "PATCH"
}


public enum RequestParameters{
    case url(_ : [String: Any]?)
    case body(_ :[String: Any]?)
}

public enum DataType {
    case JSON
    case Data
}

/// A simple protocol to that holds the request related properties
public protocol Request {
    var path: String {get}
    
    var pathContainsHost: Bool {get}
    
    var method: HTTPMethod {get}
    
    var parameters: RequestParameters {get}
    
    var headers: [String: Any]? {get}
    
    var dataType: DataType {get}
}
