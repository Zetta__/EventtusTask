//
//  GetEventsTask.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
import Foundation
class GetEventsTask: NetworkTask {
    //MARK: Properties
    var eventsType: String
    var page: Int
    var request: Request{
        return EventRequests.getEvents(type: eventsType, page: page)
    }
    
    //MARK: init and parent methods
    init(eventsType: String, page: Int) {
        self.eventsType = eventsType
        self.page = page
    }
    
    func execute(in dispatcher: Dispatcher, compeletion: @escaping (_ image: [Event]?, _ error: NetworkError?)->()) {
        dispatcher.execute(request: request) { (response) in
            var events = [Event]()
            switch response{
            case .json(let json):
                if let json = json{
                    for jsonObject in json{
                        events.append(Event.init(with: jsonObject))
                    }
                    compeletion(events, nil)
                }
            case .error(let error):
                compeletion(nil, error)
            default:
                break
            }
        }
    }
    
}
