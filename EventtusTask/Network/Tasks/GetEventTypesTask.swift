//
//  GetEventTypesTask.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class GetEventTypesTask: NetworkTask {
    
    //MARK: Properties
    var request: Request{
        return EventTypesRequests.getTypes
    }
    
    //MARK: Parent methods
    func execute(in dispatcher: Dispatcher, compeletion: @escaping (_ image: [EventType]?, _ error: NetworkError?)->()) {
        dispatcher.execute(request: request) { (response) in
            var types = [EventType]()
            switch response{
            case .json(let json):
                if let json = json{
                    for jsonObject in json{
                        types.append(EventType.init(with: jsonObject))
                    }
                    compeletion(types, nil)
                }
            case .error(let error):
                compeletion(nil, error)
            default:
                break
            }
        }
    }
    
}
