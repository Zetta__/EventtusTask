//
//  NetworkTask.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 2/16/18.
//  Copyright © 2018 zetta. All rights reserved.
//

import Foundation
/// An abstraction for any network task, it holds the request property (which holds) the request data, and contains a single function responsible for executing the request in a given dispatcher.
public protocol NetworkTask {
    associatedtype Output
    
    var request: Request {get}
    
    func execute(in dispatcher: Dispatcher, compeletion: @escaping (_ output: Output?, _ error: NetworkError?) -> ())
}
