//
//  EventsViewController.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import MBProgressHUD
class EventsViewController: UIViewController, EventsViewModelDelegate, EventTypesCollectionViewHandlerDelegate {

    //MARK: Properties
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var typesCollectionView: UICollectionView!
    var viewModel = EventsViewModel()
    var typesCollectionViewHandler: EventTypesCollectionViewHandler?
    var eventsCollectionViewHandler: EventsCollectionViewHandler?
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTypesData()
        viewModel.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCollectionViews()
    }


}
//MARK: Helper methods
extension EventsViewController{
 
    func loadTypesData() {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewModel.loadTypes()
    }
    
    func setupCollectionViews()  {
        typesCollectionViewHandler = EventTypesCollectionViewHandler(collectionView: typesCollectionView)
        typesCollectionViewHandler?.delegate = self
        eventsCollectionViewHandler = EventsCollectionViewHandler(collectionView: itemsCollectionView)
    }
    
}
//MARK: EventTypesCollectionViewHandlerDelegate method
extension EventsViewController{
    func didChooseTypeAt(indexPath: IndexPath) {
        itemsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}

//MARK: EventsViewModelDelegatec methods
extension EventsViewController{
    func didFailToLoadEventTypes(error: NetworkError) {
        DispatchQueue.main.async() {
            self.typesCollectionView.reloadData()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func didLoadEventTypesSuccessfully(typesViewModls: [EventTypeViewModel], listViewModels: [EventListViewModel]) {
        DispatchQueue.main.async() {
            self.typesCollectionViewHandler?.updateViewModels(viewModels: typesViewModls)
            self.eventsCollectionViewHandler?.updateViewModels(viewModels: listViewModels)
            MBProgressHUD.hide(for: self.view, animated: true)
            
        }
    }
}
