//
//  ItemsCollectionViewCell.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import MBProgressHUD
class ItemsCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource, EventListViewModelDelegate {
    
    //MARK: Properties
    @IBOutlet weak var itemsTableView: UITableView!
    var viewModel: EventListViewModel?
    var itemsViewModels = [ItemViewModel]()
    var shouldLoadDate = false
    var shouldShowLoading = false
    
    //MARK: Public methods
    override func prepareForReuse() {
        super.prepareForReuse()
        itemsViewModels = [ItemViewModel]()
        itemsTableView.reloadData()
        itemsTableView.setContentOffset(.zero, animated: false)

    }

    func loadDateWithViewModel(listViewModel: EventListViewModel)  {
        setupTableView()
        viewModel = listViewModel
        viewModel?.delegate = self
        shouldLoadDate = true
        itemsViewModels = listViewModel.itemsViewModels
        itemsTableView.reloadData()

        // To be sure that the user is not fast swiping
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.fetchData()
        })
        
    }
    
    //MARK: Helper methods
    private func setupTableView()  {
        let cellNib = UINib(nibName: "EventTableViewCell", bundle: nil)
        itemsTableView.register(cellNib, forCellReuseIdentifier: "eventCell")
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
        itemsTableView.tableFooterView = UIView()
        itemsTableView.separatorStyle = .none
    }
    
    private func fetchData()  {
        if shouldLoadDate{
            viewModel?.loadItems()
            MBProgressHUD.showAdded(to: self, animated: true)
        }
    }
}

//MARK: TableViewDelegates
extension ItemsCollectionViewCell{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventTableViewCell
        let itemViewModel = itemsViewModels[indexPath.row]
        cell.loadDateWithViewModel(itemViewModel: itemViewModel)
        return cell
    }
}

//MARK: ScrollView delegates and helpers
extension ItemsCollectionViewCell{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((itemsTableView.contentOffset.y + itemsTableView.frame.size.height) >= itemsTableView.contentSize.height)
        {
            if shouldShowLoading{
                shouldShowLoading = false
                showIndicator()
                viewModel?.loadNextPage()
            }
        }
    }
    
    func showIndicator()  {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.startAnimating()
        indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: itemsTableView.bounds.width, height: CGFloat(44))
        
        self.itemsTableView.tableFooterView = indicator
        self.itemsTableView.tableFooterView?.isHidden = false
    }
    
}

//MARK: EventListViewModelDelegate methods
extension ItemsCollectionViewCell{
    func didFinishLoadingItems(items: [ItemViewModel], currentPage: Int, totalNumberOfPages: Int) {
        DispatchQueue.main.async() {
            MBProgressHUD.hide(for: self, animated: true)
            self.itemsViewModels = items
            self.itemsTableView.tableFooterView?.isHidden = true
            self.shouldShowLoading = currentPage < totalNumberOfPages
            self.itemsTableView.reloadData()
        }
    }

    func didFaileToLoadItems(error: NetworkError) {
        DispatchQueue.main.async() {
            MBProgressHUD.hide(for: self, animated: true)
            self.shouldShowLoading = true
            self.itemsTableView.tableFooterView?.isHidden = true
        }
    }
}
