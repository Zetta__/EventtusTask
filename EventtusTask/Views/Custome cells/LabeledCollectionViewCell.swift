//
//  LabeledCollectionViewCell.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class LabeledCollectionViewCell: UICollectionViewCell {
    //MARK: Properties
    @IBOutlet weak var textLabel: UILabel!
    //MARK: Public methods
    func loadDateWithViewModel(typeViewModel: EventTypeViewModel){
        textLabel.text = typeViewModel.name
    }
}
