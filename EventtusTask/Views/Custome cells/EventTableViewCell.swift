//
//  EventTableViewCell.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SDWebImage
class EventTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK: Public methods
    func loadDateWithViewModel(itemViewModel: ItemViewModel){
        nameLabel.text = itemViewModel.name
        dateLabel.text = itemViewModel.startDate
        mainImageView.sd_setImage(with: itemViewModel.cover, completed: nil)
    }

}
