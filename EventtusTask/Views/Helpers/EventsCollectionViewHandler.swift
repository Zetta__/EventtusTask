//
//  EventsCollectionViewHandler.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/18/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
class EventsCollectionViewHandler:NSObject, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //MARK: Properties
    var collectionView: UICollectionView
    var viewModels = [EventListViewModel]()
    private var reuseIdentfier = "itemsCell"
    
    //MARK: init and public methods
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        registerCollectionViewWithNib()
        updateFlowLayout()

        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func updateViewModels(viewModels: [EventListViewModel])  {
        self.viewModels = viewModels
        collectionView.reloadData()
    }
}

//MARK: CollectionViewDelegate and DataSource
extension EventsCollectionViewHandler{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! ItemsCollectionViewCell).shouldLoadDate = false
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentfier, for: indexPath) as!
        ItemsCollectionViewCell
        let listViewModel = viewModels[indexPath.row]
        cell.loadDateWithViewModel(listViewModel: listViewModel)
        return cell
    }
}

//MARK: Helper methods
extension EventsCollectionViewHandler{
    private func registerCollectionViewWithNib(){
        let itemsCell = UINib(nibName: "ItemsCollectionViewCell", bundle: nil)
        collectionView.register(itemsCell, forCellWithReuseIdentifier: reuseIdentfier)
    }
    
    private func updateFlowLayout(){
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    
}
