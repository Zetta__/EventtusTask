//
//  EventTypesHandler.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/18/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
protocol EventTypesCollectionViewHandlerDelegate: class{
    func didChooseTypeAt(indexPath: IndexPath)
}

class EventTypesCollectionViewHandler:NSObject, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  
    //MAKR: Properties
    var collectionView: UICollectionView
    var viewModels = [EventTypeViewModel]()
    private var reuseIdentfier = "labeledCell"
    weak var delegate: EventTypesCollectionViewHandlerDelegate?
    
    //MARK: init and public methods
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        registerCollectionViewWithNib()
        updateFlowLayout()
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func updateViewModels(viewModels: [EventTypeViewModel])  {
        self.viewModels = viewModels
        collectionView.reloadData()
    }
}

//MARK: CollectionViewDelegate and DataSource
extension EventTypesCollectionViewHandler{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentfier, for: indexPath) as!
        LabeledCollectionViewCell
        let viewModel = viewModels[indexPath.row]
        cell.loadDateWithViewModel(typeViewModel: viewModel)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didChooseTypeAt(indexPath: indexPath)
    }
}

//MARK: Helper methods
extension EventTypesCollectionViewHandler{
    private func registerCollectionViewWithNib(){
        let typesCell = UINib(nibName: "LabeledCollectionViewCell", bundle: nil)
        collectionView.register(typesCell, forCellWithReuseIdentifier: reuseIdentfier)
    }
    
    private func updateFlowLayout(){
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        }
    }
}
