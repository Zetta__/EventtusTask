//
//  RoundedCornersView.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/18/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedCornersView: UIView {
    //MARK: Properties
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor?{
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }

}
