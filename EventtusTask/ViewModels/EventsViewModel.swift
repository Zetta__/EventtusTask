//
//  EventListViewModel.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation

protocol EventsViewModelDelegate: class{
    func didLoadEventTypesSuccessfully(typesViewModls: [EventTypeViewModel], listViewModels: [EventListViewModel])
    func didFailToLoadEventTypes(error: NetworkError)
}


class EventsViewModel {
    //MARK: Properties
    var typesViewModels = [EventTypeViewModel]()
    var types = [EventType]()
    var eventListViewModels = [EventListViewModel]()
    weak var delegate: EventsViewModelDelegate?

    //MARK: Public methods
    func loadTypes()  {
        let manager = APIManager.testManager
        manager.getEventsTypes {[weak self] (types, error) in
            guard error == nil else{
                self?.reportFailedTypesLoading(error: error!)
                return
            }
            var typeViewModels = [EventTypeViewModel]()
            var listViewModles = [EventListViewModel]()
            for type in types!{
                typeViewModels.append(EventTypeViewModel(type: type))
                listViewModles.append(EventListViewModel(type: type.typeId))
            }
            self?.typesViewModels = typeViewModels
            self?.eventListViewModels = listViewModles
            self?.types = types!
            self?.reportSuccessfullTypesLoading(types: typeViewModels, listViewModels: listViewModles)
        }
    }
    
}

//MARK: Reporting methods
extension EventsViewModel{
    func reportSuccessfullTypesLoading(types: [EventTypeViewModel], listViewModels: [EventListViewModel])  {
        self.delegate?.didLoadEventTypesSuccessfully(typesViewModls: types, listViewModels: listViewModels)
    }
    
    func reportFailedTypesLoading(error: NetworkError){
        self.delegate?.didFailToLoadEventTypes(error: error)
    }
}

