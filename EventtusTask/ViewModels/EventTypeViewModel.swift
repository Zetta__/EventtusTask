//
//  EventTypeViewModel.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class EventTypeViewModel {
    //MARK: Properties
    private var type: EventType
    var name: String{
        return type.name
    }
    //MARK: init
    init(type: EventType) {
        self.type = type
    }
    

}
