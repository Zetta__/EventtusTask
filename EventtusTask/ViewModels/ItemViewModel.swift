//
//  ItemViewModel.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
class ItemViewModel {
    //MARK: Properties
    private var event: Event
    var name: String{
        return event.name
    }
    
    var cover: URL?{
        return event.coverURL
    }
    
    var startDate: String{
        return event.startDate
    }
    
    //MARK: init
    init(event: Event) {
        self.event = event
    }

    

}
