//
//  EventListViewModel.swift
//  EventtusTask
//
//  Created by Abdelrahman Badary on 5/17/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
//MARK: Delegate protocol
protocol EventListViewModelDelegate {
    func didFinishLoadingItems(items: [ItemViewModel], currentPage: Int, totalNumberOfPages: Int)
    func didFaileToLoadItems(error: NetworkError)
}

class EventListViewModel {
    //MARK: Properties
    var currentPage = 1
    let totalNumberOfPages = 3
    var itemsViewModels = [ItemViewModel]()
    var delegate: EventListViewModelDelegate?
    var type: String
    
    //MARK: init and public methods
    init(type: String) {
        self.type = type
    }
    
    func loadItems(){
        if(itemsViewModels.count == 0){
            loadDate()
        }
        else{
            self.reportSuccessfullTypesLoading(items: itemsViewModels)
        }
    }
    
    func loadNextPage(){
        currentPage = currentPage + 1
        if currentPage <= totalNumberOfPages{
            loadDate(page: currentPage)
        }
        else{
            self.reportSuccessfullTypesLoading(items: itemsViewModels)
        }
    }
    
    //MARK: Helper methods
    private func loadDate(page: Int = 1){
        let manager = APIManager.testManager
        manager.getEvents(type: type, page: currentPage) {[weak self] (events, error) in
            
            guard error == nil else{
                self?.reportFailedTypesLoading(error: error!)
                return
            }
            var itemsViewModels = [ItemViewModel]()
            for event in events!{
                itemsViewModels.append(ItemViewModel(event: event))
            }
            self?.itemsViewModels.append(contentsOf: itemsViewModels)
            self?.reportSuccessfullTypesLoading(items: (self?.itemsViewModels)!)
        }
    }
}

//MARK: Reporting methods
extension EventListViewModel{
    func reportSuccessfullTypesLoading(items: [ItemViewModel])  {
        self.delegate?.didFinishLoadingItems(items: items, currentPage: currentPage, totalNumberOfPages: totalNumberOfPages)
    }
    
    func reportFailedTypesLoading(error: NetworkError){
        self.delegate?.didFaileToLoadItems(error: error)
        if self.currentPage > 1 {
            self.currentPage -= 1
        }
    }
}
